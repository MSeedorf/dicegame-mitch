/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.cavero.dicegame;

import nl.cavero.diceutils.Dice;
import nl.cavero.diceutils.DiceSide;

/**
 *
 * @author Mitchell
 */
public class Dicegame {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        System.out.println("Dicegame started");
        
        Dice dice1 = new Dice();
        DiceSide diceSide = dice1.roll();
        
        Dice dice2 = new Dice();
        DiceSide diceSide2 = dice2.roll();
       
        
        int x, y, z;
        x = diceSide.getSideNumber();
        y = diceSide2.getSideNumber();
        z = x + y;
        
        System.out.println("Your dice rolled :" + diceSide.getSideNumber() + " Out of ");
        System.out.println("Your dice rolled :" + diceSide2.getSideNumber());
        System.out.println("That makes a total of :" +z);
    }
    

    
}
