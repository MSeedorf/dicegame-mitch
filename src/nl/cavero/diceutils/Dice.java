/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.cavero.diceutils;


/**
 *
 * @author Mitchell
 */
public class Dice {

    private int count;
    public DiceSide[] sides;
    public Integer allSides;

    public Dice() {
        sides = new DiceSide[6];
        //vul mbv een for loop: array sides met 6 nieuw aangemaakte DiceSides.
        
        for (int x = 0; x < sides.length; x++) {
            // Do something with values[i], such as print it
            DiceSide diceSide = new DiceSide();
            diceSide.setSideNumber(x + 1);
            sides[x] = diceSide;
//            System.out.println("Side of Dice = " + sides[x].getSideNumber());
        }
      
        //System.out.println(randomWithRange(1, 6));
        //System.out.println(this.roll());
    }
    
    

    public int randomWithRange(int min, int max) {
        //zie javadoc voor random getal, begrens deze tussen 0 en 5
        //return het gegenereerde getal 
        int range = (max - min) + 1;
        double d = (Math.random());
//        int iRandom = (int) d;
//        iRandom = (iRandom * range);
        return (int) (Math.random() * range) + min - 1;
    }

    public DiceSide roll() {
        int randomNumber = (this.randomWithRange(1, 6));
        DiceSide side = sides[randomNumber];
        //side = array[getal];      
        //genereer een random getal
        //gebruik dit random getal om een side uit de array te halen
        
        return side;
    }    

    /**
     * @return the count
     */
    public int getCount() {
        return count;
    }

    /**
     * @param count the count to set
     */
    public void setCount(int count) {
        this.count = count;
    }
    
    public int allSides() {
        return sides.length;
    }

    /**
     * @return the allSides
     */
    public Integer getAllSides() {
        return allSides;
    }

    /**
     * @param allSides the allSides to set
     */
    public void setAllSides(Integer allSides) {
        this.allSides = allSides;
    }

}
