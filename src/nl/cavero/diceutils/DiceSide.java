/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package nl.cavero.diceutils;

/**
 *
 * @author Mitchell
 */
public class DiceSide {
    private String description;
    private Integer sideNumber;   
    public DiceSide() {
        
    }
    
    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the sideNumber
     */
    public Integer getSideNumber() {
        return sideNumber;
    }

    /**
     * @param sideNumber the sideNumber to set
     * He Mitchell!!!!!
     */
    public void setSideNumber(Integer sideNumber) {
        this.sideNumber = sideNumber;
    }
    
    
}
